# TuxPublish

TuxPublish (formally known as LLP-L) is a flask based application which builds a directory listing from S3.

URL strings are based on /folder/ in the S3 bucket.

# Configuration

After filling in config.py run the following for a local dev env:

```shell
virtualenv -p python3 venv --always-copy
source venv/bin/activate
export FLASK_APP=app.py
````

# Lambda/Zappa

This application is intended to be ran and deployed with Zappa/Lambda.

To setup zappa run:

```shell
zappa init
zappa deploy dev
```

# JSON: Return a list of files in a JSON Array

It is possible to return a list of files in a JSON array.

curl -X POST http://localhost/path/to/files

The important bit to note is -X POST, as this is the JSON switcher.
