from flask import Flask, render_template, request, redirect, url_for, flash, \
Response, session, jsonify, make_response
from flask_bootstrap import Bootstrap
from filters import datetimeformat, file_type, file_name, file_size, folder_name
from resources import gen_s3_folders, gen_s3_files, create_signed_url

app = Flask(__name__)
Bootstrap(app)
app.secret_key = 'secret'
app.jinja_env.filters['datetimeformat'] = datetimeformat
app.jinja_env.filters['file_type'] = file_type
app.jinja_env.filters['file_name'] = file_name
app.jinja_env.filters['file_size'] = file_size
app.jinja_env.filters['folder_name'] = folder_name



@app.route('/<path:dummy>', methods=['GET', 'POST'])
def fallback(dummy):
    url = dummy
    if not url.endswith('/'):
        url += '/'
    files = []
    folders = []
    json = []

    gen_folders = gen_s3_folders(prefix=url)

    for fol in gen_folders:
        if url in fol:
            folders.append(fol)

    for file in gen_s3_files(prefix=url):
        if file['Size'] != 0:
            files.append(file)
            jsond = {
            'Url':request.base_url + file['Key'],
            'Size': file['Size'],
            'LastModified': file['LastModified'],
            'ETag': file['ETag']
            }
            json.append(jsond)

            if file['Key'] == url:
                return create_signed_url(url)

    if request.method == 'POST':
        return make_response(jsonify(json), 200)

    if files or folders:
        return render_template('files.html', files=files, folders=folders)
    else:
        return render_template('404.html'), 404

@app.route('/')
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run()
