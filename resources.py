import boto3
from config import S3_BUCKET, S3_KEY, S3_SECRET, S3_REGION
from flask import session, redirect

def _get_s3_client():
    if S3_KEY and S3_SECRET:
        return boto3.client(
        's3',
        region_name=S3_REGION
        )

def gen_s3_folders(prefix=''):
    client = _get_s3_client()
    bucket = S3_BUCKET
    paginator = client.get_paginator('list_objects_v2')
    for result in paginator.paginate(Bucket=bucket, Prefix=prefix,
                                     Delimiter='/'):
        for prefix in result.get('CommonPrefixes', []):
            yield prefix.get('Prefix')

def gen_s3_files(prefix=''):
    bucket = S3_BUCKET
    s3 = _get_s3_client()
    paginator = s3.get_paginator('list_objects_v2')

    kwargs = {'Bucket': bucket}

    if isinstance(prefix, str):
        prefixes = (prefix, )
    else:
        prefixes = prefix

    for key_prefix in prefixes:
        kwargs['Prefix'] = key_prefix

        for page in paginator.paginate(Bucket=bucket, Prefix=prefix,
                                       Delimiter='/'):
            try:
                contents = page['Contents']
            except KeyError:
                return

            for obj in contents:
                yield obj


def create_signed_url(resource):
        s3 = _get_s3_client()
        url = s3.generate_presigned_url('get_object',
        Params={'Bucket': S3_BUCKET, 'Key': resource}, ExpiresIn=90)
        return redirect(url, code=302)
